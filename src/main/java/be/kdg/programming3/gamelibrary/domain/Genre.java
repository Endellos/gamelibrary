package be.kdg.programming3.gamelibrary.domain;




public enum Genre {
    SANDBOX ("Sandbox"),
    RTS ("Real Time Strategy"),
    SHOOTER ("Shooter"),
    MOBA ("Multiplayer online battle arena"),
    RPG("Role-playing game"),
    PLATFORMER("Platformer"),
    MMORPG ("Massively multiplayer online role-playing game"),
    ACTION_ADVENTURE ("Action Adventure");

     String name;

    Genre(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}
