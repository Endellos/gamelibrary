package be.kdg.programming3.gamelibrary.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "GAMES")
public class Game {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;



    private String name;
    @JsonManagedReference
    @ManyToOne(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinColumn(name = "PUBLISHER",referencedColumnName = "ID")
    private Publisher publisher;

    @Enumerated(EnumType. STRING)
    private Genre genre;
    private LocalDate releaseDate;
    private double price;
    private int copiesSold;
    @ManyToMany
    @JoinTable(
            name = "GAME_PLATFORM",
            joinColumns = @JoinColumn( name = "GAME_ID"),
            inverseJoinColumns = @JoinColumn( name = "PLATFORM_ID" )

    )

    private  List<Platform> platforms = new ArrayList<>();

    public Game() {
    }

    public Game(String name, Publisher publisher, Genre genre, LocalDate releaseDate, double price, int copiesSold) {
        this.name = name;
       this.publisher = publisher;
        this.genre = genre;
        this.releaseDate = releaseDate;
        this.price = price;
        this.copiesSold = copiesSold;
    }

    public Game(String name, Genre genre, LocalDate releaseDate, double price, int copiesSold) {
        this.name = name;
        this.genre = genre;
        this.releaseDate = releaseDate;
        this.price = price;
        this.copiesSold = copiesSold;
    }
    public Game(String name, LocalDate releaseDate, double price, int copiesSold) {
        this.name = name;

        this.releaseDate = releaseDate;
        this.price = price;
        this.copiesSold = copiesSold;
    }

    public Game(int id, String name, Publisher publisher, Genre genre, LocalDate releaseDate, double price, int copiesSold) {
        this.id = id;
        this.name = name;
        this.publisher = publisher;
        this.genre = genre;
        this.releaseDate = releaseDate;
        this.price = price;
        this.copiesSold = copiesSold;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void addPlatform(Platform object){
        if (platforms==null) platforms = new ArrayList<>();
        platforms.add(object);
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getCopiesSold() {
        return copiesSold;
    }

    public void setCopiesSold(int copiesSold) {
        this.copiesSold = copiesSold;
    }

    public List<Platform> getPlatforms() {
        return platforms;
    }

    public void setPlatforms(List<Platform> platforms) {
        this.platforms = platforms;
    }

    @Override
    public String toString() {
        return String.format("Name %s, Publisher %s", getName(), getPublisher());
    }
}
