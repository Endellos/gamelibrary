package be.kdg.programming3.gamelibrary.domain;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
@Entity
@Table(name = "platforms")
public class Platform {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String name;

    @Enumerated(EnumType. STRING)
    private PlatformDevelopers developer;
    @Column(name = "RELEASE_DATE")
    private LocalDate releaseDate;
    private int generation;

    @ManyToMany(mappedBy = "platforms",fetch = FetchType.EAGER ,cascade = {CascadeType.PERSIST, CascadeType.MERGE, CascadeType.REFRESH,  CascadeType.DETACH})
    private  List<Game> games = new ArrayList<>();



    public List<Game> getGames() {
        return games;
    }

    public void setGames(List<Game> games) {
        this.games = games;
    }

    public PlatformDevelopers getDeveloper() {
        return developer;
    }

    public Platform() {
    }

    public Platform(String name, PlatformDevelopers developer, LocalDate releaseDate, int generation) {
        this.name = name;
        this.developer = developer;
        this.releaseDate = releaseDate;
        this.generation = generation;
    }

    public Platform(int id, String name, PlatformDevelopers developer, LocalDate releaseDate, int generation) {
        this.id = id;
        this.name = name;
        this.developer = developer;
        this.releaseDate = releaseDate;
        this.generation = generation;
    }

    public void addGame(Game object) {
        if (games == null) games = new ArrayList<>();
        games.add(object);
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }


    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public int getGeneration() {
        return generation;
    }

    @Override
    public String toString() {
        return "name='" + name + '\'' +
                ", developer=" + developer +
                ", releaseDate=" + releaseDate +
                ", generation=" + generation;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setDeveloper(PlatformDevelopers developer) {
        this.developer = developer;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public void setGeneration(int generation) {
        this.generation = generation;
    }


}
