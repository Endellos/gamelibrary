package be.kdg.programming3.gamelibrary.domain;

public enum PlatformDevelopers {
    SONY("Sony"),
    MICROSOFT("Microsoft"),
    NINTENDO("Nintendo"),
    GOOGLE("Google");


    String name;

    PlatformDevelopers(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }
}

