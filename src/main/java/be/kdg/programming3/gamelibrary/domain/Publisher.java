package be.kdg.programming3.gamelibrary.domain;

import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "PUBLISHERS")
public class Publisher {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)

    private int id;

    @Column(name = "PUBLISHER_NAME")
    private String publisherName;
    private String headquarters;
    private LocalDate founded;
    @JsonBackReference
    @OneToMany(mappedBy = "publisher")
    private  List<Game> games = new ArrayList<>();

    public Publisher(String name, String headquarters, LocalDate founded) {
        this.publisherName = name;
        this.headquarters = headquarters;
        this.founded = founded;
    }

    public Publisher() {

    }

    public Publisher(int id, String publisherName, String headquarters, LocalDate founded) {
        this.id = id;
        this.publisherName = publisherName;
        this.headquarters = headquarters;
        this.founded = founded;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void addGame(Game game) {
        this.games.add(game);
    }

    public String getPublisherName() {
        return publisherName;
    }

    public void setPublisherName(String publisherName) {
        this.publisherName = publisherName;
    }

    public String getHeadquarters() {
        return headquarters;
    }

    public void setHeadquarters(String headquarters) {
        this.headquarters = headquarters;
    }

    public LocalDate getFounded() {
        return founded;
    }

    public void setFounded(LocalDate founded) {
        this.founded = founded;
    }

    public List<Game> getGames() {
        return games;
    }

    public void setGames(List<Game> games) {
        this.games = games;
    }

    @Override
    public String toString() {
        return " " + publisherName;
    }
}
