package be.kdg.programming3.gamelibrary;

import be.kdg.programming3.gamelibrary.presentation.Menu;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

@SpringBootApplication

public class GameLibraryApplication {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(GameLibraryApplication.class, args);
       // Menu menu = context.getBean(Menu.class);
        //menu.show();


    }

}
