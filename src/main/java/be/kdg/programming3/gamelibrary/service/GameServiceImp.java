package be.kdg.programming3.gamelibrary.service;

import be.kdg.programming3.gamelibrary.domain.Game;
import be.kdg.programming3.gamelibrary.domain.Genre;
import be.kdg.programming3.gamelibrary.domain.Publisher;
import be.kdg.programming3.gamelibrary.exceptions.ObjectDoesNotExistException;
import be.kdg.programming3.gamelibrary.repository.GameRepository;
import be.kdg.programming3.gamelibrary.repository.JSonWriter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.List;

@Component
@Profile({"jdbc", "jpa", "collections"})
public class GameServiceImp implements GameService{


    private GameRepository gameRepository;
    private JSonWriter jSonWriter;

@Autowired
    public GameServiceImp(GameRepository gameRepository, JSonWriter jSonWriter) {
        this.gameRepository = gameRepository;
        this.jSonWriter = jSonWriter;
    }

    @Override
    public List<Game> getAllGames() {
      return gameRepository.readGames();
    }

    @Override
    public List<Game> getGameFromPublisher(Publisher publisher) {
        return gameRepository.readGames().stream().filter(game -> game.getPublisher().equals(publisher)).toList();
    }

    @Override
    public List<Publisher> getPublishers() {
       return gameRepository.getPublisherList();
    }

    @Override
    public void writeGameToJson(List<Game> games) {
        jSonWriter.writeGames(games);
    }

    @Override
    public Game addGame(String name, Publisher publisher, Genre genre, LocalDate releaseDate, double price, int copiesSold) {
//        Game g = new Game(name, publisher, genre, releaseDate, price, copiesSold);
        return gameRepository. createGames( new Game(name, publisher, genre, releaseDate, price, copiesSold));
    }

    @Override
    public Game addGame(Game game) {
        return gameRepository.createGames(game);
    }

    @Override
    public Game findByName(String name) {
        return gameRepository.findByName(name);
    }

    @Override
    public Game findById(int id) {
    if (gameRepository.findById(id)==null){
        throw new ObjectDoesNotExistException("Game with id "+ id+" does not exist");
    }
        return gameRepository.findById(id);
    }

    @Override
    public List<Game> findByPlatform(int platformID) {
       return gameRepository.findByPlatform(platformID);
    }

    @Override
    public void deleteGame(int id) {
        gameRepository.deleteGames(id);
    }


}
