package be.kdg.programming3.gamelibrary.service.springdata;

import be.kdg.programming3.gamelibrary.domain.User;
import be.kdg.programming3.gamelibrary.repository.springdata.SpringDataUserRepository;
import be.kdg.programming3.gamelibrary.service.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class SpringDataUserService implements UserService {

    SpringDataUserRepository userRepository;

    @Autowired
    public SpringDataUserService(SpringDataUserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public User getUserByName(String username) {
        return userRepository.findByUsername(username);
    }
}
