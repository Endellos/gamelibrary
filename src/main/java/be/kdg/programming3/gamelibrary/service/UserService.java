package be.kdg.programming3.gamelibrary.service;

import be.kdg.programming3.gamelibrary.domain.User;

public interface UserService {
    public User getUserByName(String username);
}
