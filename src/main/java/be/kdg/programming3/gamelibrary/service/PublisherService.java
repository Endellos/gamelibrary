package be.kdg.programming3.gamelibrary.service;


import be.kdg.programming3.gamelibrary.domain.Publisher;
import be.kdg.programming3.gamelibrary.repository.PublisherRepository;
import org.springframework.data.repository.query.Param;

import java.time.LocalDate;
import java.util.List;

public interface PublisherService {




    List<Publisher> getPublishers();
    Publisher addPublisher(String name, String headquarters, LocalDate founded);

    Publisher findById(int id);

    void updatePublisher(int id, String publisherName,String headquarters, LocalDate founded);


}
