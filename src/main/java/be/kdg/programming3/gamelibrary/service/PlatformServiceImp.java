package be.kdg.programming3.gamelibrary.service;

import be.kdg.programming3.gamelibrary.domain.Platform;
import be.kdg.programming3.gamelibrary.domain.PlatformDevelopers;
import be.kdg.programming3.gamelibrary.exceptions.ObjectDoesNotExistException;
import be.kdg.programming3.gamelibrary.repository.JSonWriter;
import be.kdg.programming3.gamelibrary.repository.PlatformRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
@Profile({"jdbc", "jpa", "collections"})
public class PlatformServiceImp implements PlatformService{

    private PlatformRepository platformRepository;
    private JSonWriter jSonWriter;

    public PlatformServiceImp(PlatformRepository platformRepository, JSonWriter jSonWriter) {
        this.platformRepository = platformRepository;
        this.jSonWriter = jSonWriter;
    }

    @Override
    public List<Platform> getPlatforms() {
        return platformRepository.readPlatforms();
    }

    @Override
    public List<Platform> getPlatformsWithGenAndDev(int generation, PlatformDevelopers developer) {
        List<Platform> platforms = new ArrayList<>();
        if (generation>1){platforms=platformRepository.readPlatforms().stream().filter(platform -> platform.getGeneration()<generation).collect(Collectors.toList());}
        if (developer!=null){platforms=platformRepository.readPlatforms().stream().filter(platform -> platform.getDeveloper()==developer).collect(Collectors.toList());}

        return platforms;
    }

    @Override
    public void writePlatformToJson(List<Platform> platformList) {
        jSonWriter.writePlatform(platformList);
    }

    @Override
    public Platform addPlatform(String name, PlatformDevelopers developer, LocalDate releaseDate, int generation) {
        Platform p= new Platform(name,developer,releaseDate, generation);
        return platformRepository.createPlatform(p);
    }

    @Override
    public Platform addPlatform(Platform platform) {
        return platformRepository.createPlatform(platform);
    }

    @Override
    public Platform findByName(String name) {
        return platformRepository.findByName(name);
    }

    @Override
    public Platform findById(int id) {
        if (platformRepository.findById(id)==null){
            throw new ObjectDoesNotExistException("Platform with id "+ id+" does not exist");
        }
        return platformRepository.findById(id);
    }

    @Override
    public void deletePlatform(int id) {
        platformRepository.deletePlatform(id);
    }
}
