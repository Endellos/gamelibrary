package be.kdg.programming3.gamelibrary.service.springdata;

import be.kdg.programming3.gamelibrary.domain.Game;
import be.kdg.programming3.gamelibrary.domain.Genre;
import be.kdg.programming3.gamelibrary.domain.Publisher;
import be.kdg.programming3.gamelibrary.exceptions.ObjectDoesNotExistException;
import be.kdg.programming3.gamelibrary.repository.JSonWriter;
import be.kdg.programming3.gamelibrary.repository.springdata.SpringDataGameRepository;
import be.kdg.programming3.gamelibrary.service.GameService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@Profile("springData")
public class SpringDataGameService implements GameService
{

    private JSonWriter jSonWriter;
    private SpringDataGameRepository gameRepository;

    public SpringDataGameService(JSonWriter jSonWriter, SpringDataGameRepository gameRepository) {
        this.jSonWriter = jSonWriter;
        this.gameRepository = gameRepository;
    }

    @Override
    public List<Game> getAllGames() {
       return gameRepository.findAll();
    }

    @Override
    public List<Game> getGameFromPublisher(Publisher publisher) {
        return gameRepository.findByPublisher(publisher);
    }

    @Override
    public List<Publisher> getPublishers() {
        return gameRepository.findAllPublishers();
    }

    @Override
    public void writeGameToJson(List<Game> games) {
        jSonWriter.writeGames(games);
    }

    @Override
    public Game addGame(String name, Publisher publisher, Genre genre, LocalDate releaseDate, double price, int copiesSold) {
        return gameRepository.save(new Game(name,publisher,genre,releaseDate,price, copiesSold));
    }

    @Override
    public Game addGame(Game game) {
        return gameRepository.save(game);
    }

    @Override
    public Game findByName(String name) {
        return gameRepository.findByName(name).get();
    }

    @Override
    public Game findById(int id) {
        if (!gameRepository.findById(id).isPresent()){
            throw new ObjectDoesNotExistException("Game with id "+ id+" does not exist");
        }
        return gameRepository.findById(id).get();
    }

    @Override
    public List<Game> findByPlatform(int platformID) {
        return gameRepository.findByPlatforms_Id(platformID);
    }



    @Override
    public void deleteGame(int id) {
    gameRepository.delete(gameRepository.findById(id).get());
    }

}
