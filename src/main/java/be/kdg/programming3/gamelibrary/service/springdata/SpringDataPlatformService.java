package be.kdg.programming3.gamelibrary.service.springdata;

import be.kdg.programming3.gamelibrary.domain.Platform;
import be.kdg.programming3.gamelibrary.domain.PlatformDevelopers;
import be.kdg.programming3.gamelibrary.exceptions.ObjectDoesNotExistException;
import be.kdg.programming3.gamelibrary.repository.JSonWriter;
import be.kdg.programming3.gamelibrary.repository.springdata.SpringDataPlatformRepository;
import be.kdg.programming3.gamelibrary.service.PlatformService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@Profile("springData")
public class SpringDataPlatformService implements PlatformService {

    private SpringDataPlatformRepository platformRepository;
    private JSonWriter jSonWriter;

    public SpringDataPlatformService(SpringDataPlatformRepository platformRepository, JSonWriter jSonWriter) {
        this.platformRepository = platformRepository;
        this.jSonWriter = jSonWriter;
    }

    @Override
    public List<Platform> getPlatforms() {
        return platformRepository.findAll();
    }

    @Override
    public List<Platform> getPlatformsWithGenAndDev(int generation, PlatformDevelopers developer) {
        return platformRepository.findByGenerationAndDeveloper(generation,developer);
    }

    @Override
    public void writePlatformToJson(List<Platform> platformList) {
        jSonWriter.writePlatform(platformList);
    }

    @Override
    public Platform addPlatform(String name, PlatformDevelopers developer, LocalDate releaseDate, int generation) {
      Platform platform =new Platform(name,developer,releaseDate,generation);
        platform.getGames().forEach(game -> game.addPlatform(platform));
        return platformRepository.save(platform);
    }

    @Override
    public Platform addPlatform(Platform platform) {
        return platformRepository.save(platform);
    }

    @Override
    public Platform findByName(String name) {
        return platformRepository.findByName(name).get();
    }

    @Override
    public Platform findById(int id) {
        if (!platformRepository.findById(id).isPresent()){
        throw new ObjectDoesNotExistException("Platform with id "+ id+" does not exist");
    }
        return platformRepository.findById(id).get();
    }

    @Override
    public void deletePlatform(int id) {
//        Platform platform= em.find(Platform.class, id);
//        platform.getGames().forEach(game -> game.getPlatforms().remove(platform));
//        em.remove(em.find(Platform.class, id));
       Platform platform= findById(id);
        platform.getGames().forEach(game -> game.getPlatforms().remove(platform));
        platformRepository.deleteById(id);
    }
}
