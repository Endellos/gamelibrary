package be.kdg.programming3.gamelibrary.service;

import be.kdg.programming3.gamelibrary.domain.Publisher;
import be.kdg.programming3.gamelibrary.exceptions.ObjectDoesNotExistException;
import be.kdg.programming3.gamelibrary.repository.PublisherRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@Profile({"jdbc", "jpa","collections"})

public class PublisherServiceImp implements PublisherService{

    PublisherRepository publisherRepository;

    public PublisherServiceImp(PublisherRepository publisherRepository) {
        this.publisherRepository = publisherRepository;
    }

    @Override
    public List<Publisher> getPublishers() {
       return publisherRepository.readPublishers();
    }

    @Override
    public Publisher addPublisher(String name, String headquarters, LocalDate founded) {
        return publisherRepository.createPublishers(new Publisher(name,headquarters,founded));
    }

    @Override
    public Publisher findById(int id) {
        if (publisherRepository.findById(id)==null){
            throw new ObjectDoesNotExistException("Publisher with id "+ id+" does not exist");
        }
       return publisherRepository.findById(id);
    }

    @Override
    public void updatePublisher(int id, String publisherName, String headquarters, LocalDate founded) {

    }
}
