package be.kdg.programming3.gamelibrary.service;

import be.kdg.programming3.gamelibrary.domain.Game;
import be.kdg.programming3.gamelibrary.domain.Genre;
import be.kdg.programming3.gamelibrary.domain.Publisher;

import java.time.LocalDate;
import java.util.List;

public interface GameService {

    List<Game> getAllGames();

    List<Game> getGameFromPublisher(Publisher publisher);

    List<Publisher> getPublishers();

    void writeGameToJson(List<Game> games);

    Game addGame(String name, Publisher publisher, Genre genre, LocalDate releaseDate, double price, int copiesSold);

    Game addGame(Game game);

    Game findByName(final String name);
    Game findById(int id);

    List<Game> findByPlatform(int platformID);

    void deleteGame(int id);


}
