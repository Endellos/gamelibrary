package be.kdg.programming3.gamelibrary.service.springdata;

import be.kdg.programming3.gamelibrary.domain.Publisher;
import be.kdg.programming3.gamelibrary.exceptions.ObjectDoesNotExistException;
import be.kdg.programming3.gamelibrary.repository.springdata.SpringDataPublisherRepository;
import be.kdg.programming3.gamelibrary.service.PublisherService;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.List;

@Service
@Profile("springData")
public class SpringDataPublisherService implements PublisherService {
    private SpringDataPublisherRepository publisherRepository;

    public SpringDataPublisherService(SpringDataPublisherRepository publisherRepository) {
        this.publisherRepository = publisherRepository;
    }

    @Override
    public List<Publisher> getPublishers() {
      return publisherRepository.findAll();
    }

    @Override
    public Publisher addPublisher(String name, String headquarters, LocalDate founded) {
        return publisherRepository.save(new Publisher(name,headquarters,founded));
    }

    @Override
    public Publisher findById(int id) {
        if (!publisherRepository.findById(id).isPresent()){
            throw new ObjectDoesNotExistException("Publisher with id "+ id+" does not exist");
        }
        return publisherRepository.findById(id).get();
    }

    @Override
    public void updatePublisher(int id, String publisherName, String headquarters, LocalDate founded) {
        publisherRepository.updatePublisher(id, publisherName, headquarters, founded);
    }


}
