package be.kdg.programming3.gamelibrary.service;

import be.kdg.programming3.gamelibrary.domain.Platform;
import be.kdg.programming3.gamelibrary.domain.PlatformDevelopers;

import java.time.LocalDate;
import java.util.List;

public interface PlatformService {

    List<Platform> getPlatforms();
    List<Platform> getPlatformsWithGenAndDev(int generation, PlatformDevelopers developer);

    void writePlatformToJson(List<Platform> platformList);

    Platform addPlatform(String name, PlatformDevelopers developer, LocalDate releaseDate, int generation);

    Platform addPlatform(Platform platform);
    Platform findByName(String name);
    Platform findById(int id);
    void deletePlatform(int id);

}
