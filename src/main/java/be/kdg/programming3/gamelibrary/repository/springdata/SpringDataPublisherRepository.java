package be.kdg.programming3.gamelibrary.repository.springdata;

import be.kdg.programming3.gamelibrary.domain.Publisher;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.util.Optional;

@Profile("springData")
public interface SpringDataPublisherRepository extends JpaRepository<Publisher,Integer> {

    @Override
    Optional<Publisher> findById(Integer integer);
    @Transactional
    @Modifying
    @Query("update Publisher p set p.publisherName = :publisherName, p.headquarters = :headquarters, p.founded = :founded where p.id = :id")
    void updatePublisher(@Param("id") int id, @Param("publisherName") String publisherName, @Param("headquarters") String headquarters, @Param("founded") LocalDate founded);
}
