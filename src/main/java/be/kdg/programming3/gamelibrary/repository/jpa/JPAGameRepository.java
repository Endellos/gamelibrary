package be.kdg.programming3.gamelibrary.repository.jpa;

import be.kdg.programming3.gamelibrary.domain.Game;
import be.kdg.programming3.gamelibrary.domain.Publisher;
;
import be.kdg.programming3.gamelibrary.repository.GameRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import javax.transaction.Transactional;
import java.util.List;
@Profile("jpa")
@Repository
public class JPAGameRepository implements GameRepository {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Game> readGames() {
        List<Game> games = em.createQuery("select game from Game game", Game.class)
                .getResultList();
        return games;
    }

    @Override
    @Transactional
    public Game createGames(Game game) {
        em.persist(game);



        return game;
    }

    @Override
    public Game findByName(String name) {
        Game game = em.find(Game.class, name);
        return game;
    }

    @Override
    public Game findById(int id) {
        Game game = em.find(Game.class, id);
        return game;
    }

    @Override
    public void updateGame(Game game) {
        em.merge(game);
    }

    @Override
    @Transactional
    public void deleteGames(int id) {
         em.remove(em.find(Game.class, id));
    }

    @Override
    public List<Publisher> getPublisherList() {
       return em.createQuery("select publisher from Publisher publisher", Publisher.class).getResultList();
    }

    @Override
    public List<Game> findByPlatform(int platformID) {
        TypedQuery<Game> query = em.createQuery("select game from Game game join game.platforms platform where platform.id = :platform_id", Game.class);
        query.setParameter("platform_id", platformID);
        List<Game> games = query.getResultList();
        return games;
    }
}
