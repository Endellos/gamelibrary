package be.kdg.programming3.gamelibrary.repository.jpa;

import be.kdg.programming3.gamelibrary.domain.Game;
import be.kdg.programming3.gamelibrary.domain.Platform;
import be.kdg.programming3.gamelibrary.domain.Publisher;
import be.kdg.programming3.gamelibrary.repository.PublisherRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.transaction.Transactional;
import java.util.List;

@Profile("jpa")
@Repository
public class JPAPublisherRepository implements PublisherRepository {
    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Publisher> readPublishers() {
        List<Publisher> publishers = em.createQuery("select publisher from Publisher publisher", Publisher.class)
                .getResultList();
        return publishers;
    }

    @Override
    @Transactional
    public Publisher createPublishers(Publisher publisher) {
        em.persist(publisher);
        return publisher;
    }

    @Override
    @Transactional
    public void updatePublisher(Publisher publisher) {
        em.merge(publisher);
    }

    @Override
    @Transactional
    public void deletePublisher(String name) {
        em.remove(em.find(Publisher.class, name));

    }

    @Override
    public Publisher findById(int id) {
       return em.find(Publisher.class, id);
    }
}
