package be.kdg.programming3.gamelibrary.repository.jdbc;

import be.kdg.programming3.gamelibrary.domain.Game;
import be.kdg.programming3.gamelibrary.domain.Genre;
import be.kdg.programming3.gamelibrary.domain.Platform;
import be.kdg.programming3.gamelibrary.domain.Publisher;

import be.kdg.programming3.gamelibrary.repository.GameRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;

import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;


@Repository
@Profile("jdbc")
public class H2GameRepository implements GameRepository {

    private Logger logger = LoggerFactory.getLogger(this.getClass());
    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert inserter;

    @Autowired
    public H2GameRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.inserter=new SimpleJdbcInsert(jdbcTemplate).withTableName("GAMES").
                usingGeneratedKeyColumns("ID");
    }






     public  List<Publisher> getPublisherList(){
        return jdbcTemplate.query("select * from PUBLISHERS",H2PublisherRepository::mapRow);
    }


    @Override
    public List<Game> findByPlatform(int platformID) {
        List<Game> games = jdbcTemplate.query("SELECT * FROM GAMES WHERE ID IN " +
                "(SELECT GAME_ID FROM GAME_PLATFORM WHERE PLATFORM_ID = ?)", this::mapRow, platformID);

        games.forEach(this::loadPlatforms);
        return games;
    }

    private void loadPlatforms(Game game) {

        List<Platform> platforms = jdbcTemplate.query("SELECT * FROM PLATFORMS WHERE ID IN " +
                "(SELECT PLATFORM_ID FROM GAME_PLATFORM WHERE GAME_ID = ?) ", H2PlatformRepository::mapRowPlatform, game.getId());


        game.setPlatforms(platforms);
    }





    private  Game mapRow(ResultSet rs, int rowid) throws SQLException {
        int publisherId = rs.getInt("PUBLISHER");
        String genreStr = rs.getString("GENRE");
     Publisher pub=  getPublisherList().stream().filter(p -> p.getId()==publisherId).findFirst().get();
      Genre genre = Arrays.stream(Genre.values()).filter(genre1 -> genre1.toString().equals(genreStr)).findFirst().get();
      Game g = new Game(rs.getInt("ID"),rs.getString("NAME"),pub,genre,rs.getDate("RELEASE_DATE").toLocalDate(), rs.getDouble("PRICE"), rs.getInt("COPIES_SOLD"));

      return g;
    }


    @Override
    public List<Game> readGames() {

       return jdbcTemplate.query("SELECT * FROM GAMES", this::mapRow);

    }

    @Override
    public Game createGames(Game game) {
        Map<String,Object> parameters = new HashMap<>();
      parameters.put("NAME", game.getName());
  parameters.put("PUBLISHER", game.getPublisher().getId());
      parameters.put("GENRE", game.getGenre());
      parameters.put("RELEASE_DATE", game.getReleaseDate());
      parameters.put("COPIES_SOLD", game.getCopiesSold());
      parameters.put("PRICE", game.getPrice());
       game.setId(inserter.executeAndReturnKey(parameters).intValue());


      if (!game.getPlatforms().isEmpty()){
          game.getPlatforms().forEach((platform) -> jdbcTemplate.update("INSERT INTO GAME_PLATFORM (GAME_ID, PLATFORM_ID) VALUES ( ?,? )",game.getId(),platform.getId()));

      }
     return game;

    }

    @Override
    public Game findByName(String name) {
        Game targetGame= readGames().stream().filter(game -> game.getName().equals(name)).findFirst().get();
        loadPlatforms(targetGame);

        return targetGame;
    }

    @Override
    public Game findById(int id) {
        Game targetGame= readGames().stream().filter(game -> game.getId()==id).findFirst().get();

        loadPlatforms(targetGame);
        return targetGame;
    }


    @Override
    public void updateGame(Game game) {
        jdbcTemplate.update("UPDATE GAMES SET NAME=?, PUBLISHER=?,GENRE=?,RELEASE_DATE=?, COPIES_SOLD=?, PRICE=?  WHERE ID=?",
                game.getName(), game.getPublisher(),game.getGenre(), Date.valueOf(game.getReleaseDate()),game.getCopiesSold(),game.getPrice(), game.getId());
    }

    @Override
    @Transactional
    public void deleteGames(int id) {
        jdbcTemplate.update("DELETE FROM GAME_PLATFORM WHERE GAME_ID=?", id);
        jdbcTemplate.update("DELETE FROM GAMES WHERE ID=?", id);

        logger.info(id+" deleted");

    }

}
