package be.kdg.programming3.gamelibrary.repository.collections;

import be.kdg.programming3.gamelibrary.domain.Game;

import be.kdg.programming3.gamelibrary.domain.Platform;
import be.kdg.programming3.gamelibrary.domain.Publisher;
import be.kdg.programming3.gamelibrary.repository.DataFactory;
import be.kdg.programming3.gamelibrary.repository.GameRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;


import java.util.List;
import java.util.Optional;
import java.util.function.Function;

@Component
@Profile("collections")
public class ListGameRepository implements GameRepository {


    static List<Game> games = DataFactory.games;
    static  List<Platform> platforms = DataFactory.platforms;
    static  List<Publisher> publishers = DataFactory.publishers;

    @Override
    public List<Game> readGames() {
        return games;
    }

    @Override
    public Game createGames(Game game) {
    game.setId(games.size()+1);
        games.add(game);
        return game;

    }

    @Override
    public Game findByName(String name) {
        Optional<Game> targetGame = readGames().stream().filter(game -> game.getName().equals(name)).findFirst();
        return targetGame.get();
    }

    @Override
    public Game findById(int id) {
        Optional<Game> targetGame = readGames().stream().filter(game -> game.getId()==id).findFirst();
        return targetGame.get();
    }

    @Override
    public void updateGame(Game game) {

    }

    @Override
    public void deleteGames(int id) {
        games.remove(findById(id));
    }

    @Override
    public List<Publisher> getPublisherList() {
        return publishers;
    }

    @Override
    public List<Game> findByPlatform(int platformID) {
     Platform targetPlatform=  platforms.stream().filter(platform -> platform.getId()==platformID).findFirst().get();
    return targetPlatform.getGames();
    }
}

