package be.kdg.programming3.gamelibrary.repository;

import be.kdg.programming3.gamelibrary.domain.Game;
import be.kdg.programming3.gamelibrary.domain.Platform;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.springframework.stereotype.Component;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.List;

@Component
public class JSonWriter {
    private static final String GAMES_JSON = "games.json";
    private static final String PLATFORMS_JSON = "platforms.json";
    private Gson gson;

    public JSonWriter() {
        GsonBuilder builder = new GsonBuilder();
        builder.setPrettyPrinting();
        builder.registerTypeAdapter(LocalDate.class, new LocalDateSerializer());
        gson = builder.create();
    }

    public void writeGames(List<Game> games) {
        String json = gson.toJson(games);
        try (FileWriter writer = new FileWriter(GAMES_JSON)) {
            writer.write(json);
        } catch (IOException e) {
            throw new RuntimeException("Unable to save games to JSON", e);
        }
    }

    public void writePlatform(List<Platform> platforms) {
        String json = gson.toJson(platforms);
        try (FileWriter writer = new FileWriter(PLATFORMS_JSON)) {
            writer.write(json);
        } catch (IOException e) {
            throw new RuntimeException("Unable to save platforms to JSON", e);
        }
    }
}
