package be.kdg.programming3.gamelibrary.repository;

import be.kdg.programming3.gamelibrary.domain.Game;
import be.kdg.programming3.gamelibrary.domain.Platform;
import be.kdg.programming3.gamelibrary.domain.Publisher;

import java.util.List;

public interface GameRepository {

    List<Game> readGames();

    Game createGames(Game game);

    public Game findByName(final String name);
    Game findById(int id);

    public void updateGame(Game game);

    public void deleteGames(int id);

    public List<Publisher> getPublisherList();
    public List<Game> findByPlatform(int platformID);
}
