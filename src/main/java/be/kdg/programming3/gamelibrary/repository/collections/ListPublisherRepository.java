package be.kdg.programming3.gamelibrary.repository.collections;

import be.kdg.programming3.gamelibrary.domain.Publisher;
import be.kdg.programming3.gamelibrary.repository.DataFactory;
import be.kdg.programming3.gamelibrary.repository.PublisherRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
@Profile("collections")
public class ListPublisherRepository implements PublisherRepository {
    static List<Publisher> publishers = DataFactory.publishers;
    @Override
    public List<Publisher> readPublishers() {
        return publishers;
    }

    @Override
    public Publisher createPublishers(Publisher publisher) {
        publisher.setId(publishers.size()+1);
        publishers.add(publisher);

        return publisher;
    }

    @Override
    public void updatePublisher(Publisher publisher) {

    }

    @Override
    public void deletePublisher(String name) {

    }

    @Override
    public Publisher findById(int id) {
        Optional<Publisher> targetPublisher = readPublishers().stream().filter(publisher -> publisher.getId()==id).findFirst();
        return targetPublisher.get();
    }
}
