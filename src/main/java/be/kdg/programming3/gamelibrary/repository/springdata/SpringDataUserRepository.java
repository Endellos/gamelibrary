package be.kdg.programming3.gamelibrary.repository.springdata;

import be.kdg.programming3.gamelibrary.domain.User;
import org.springframework.data.jpa.repository.JpaRepository;

public interface SpringDataUserRepository extends JpaRepository<User, Integer> {
    User findByUsername(String username);


}
