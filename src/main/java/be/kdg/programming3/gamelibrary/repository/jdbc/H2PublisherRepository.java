package be.kdg.programming3.gamelibrary.repository.jdbc;

import be.kdg.programming3.gamelibrary.domain.Game;
import be.kdg.programming3.gamelibrary.domain.Genre;
import be.kdg.programming3.gamelibrary.domain.Publisher;
import be.kdg.programming3.gamelibrary.repository.PublisherRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Repository
@Profile("jdbc")
public class H2PublisherRepository implements PublisherRepository {

    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert inserter;

    public H2PublisherRepository(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
        this.inserter=new SimpleJdbcInsert(jdbcTemplate).withTableName("PUBLISHERS").usingGeneratedKeyColumns("ID");;
    }



    private  Game mapRowGame(ResultSet rs, int rowid) throws SQLException {
        int publisherId = rs.getInt("PUBLISHER");
        String genreStr = rs.getString("GENRE");
        Publisher pub=  readPublishers().stream().filter(p -> p.getId()==publisherId).findFirst().get();
        Genre genre = Arrays.stream(Genre.values()).filter(genre1 -> genre1.toString().equals(genreStr)).findFirst().get();
        Game g = new Game(rs.getInt("ID"),rs.getString("NAME"),pub,genre,rs.getDate("RELEASE_DATE").toLocalDate(), rs.getDouble("PRICE"), rs.getInt("COPIES_SOLD"));

        return g;
    }
    private void loadPublisher(Publisher publisher){
        List<Game> games =  jdbcTemplate.query("select * from GAMES where PUBLISHER =?",this::mapRowGame, publisher.getId());
        publisher.setGames(games);
    }



    public static Publisher mapRow(ResultSet rs, int rowNum) throws SQLException {
        return  new Publisher(rs.getInt("ID"), rs.getString("PUBLISHER_NAME"),rs.getString("HEADQUARTERS"),rs.getDate("FOUNDED").toLocalDate());}

    @Override
    public List<Publisher> readPublishers() {
        return jdbcTemplate.query("SELECT * FROM PUBLISHERS", H2PublisherRepository::mapRow);
    }

    @Override
    public Publisher createPublishers(Publisher publisher) {
        Map<String,Object> parameters = new HashMap<>();
        parameters.put("PUBLISHER_NAME", publisher.getPublisherName());
        parameters.put("HEADQUARTERS", publisher.getHeadquarters());
        parameters.put("FOUNDED", publisher.getFounded());
      publisher.setId(inserter.executeAndReturnKey(parameters).intValue());
      return publisher;
    }

    @Override
    public void updatePublisher(Publisher game) {

    }

    @Override
    public void deletePublisher(String name) {

    }

    @Override
    public Publisher findById(int id) {

           Publisher pub =  readPublishers().stream().filter(publisher -> publisher.getId()==id).findFirst().get();
        loadPublisher(pub);
        return pub;
    }
}
