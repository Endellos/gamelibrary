package be.kdg.programming3.gamelibrary.repository.jpa;

import be.kdg.programming3.gamelibrary.domain.Game;
import be.kdg.programming3.gamelibrary.domain.Platform;
import be.kdg.programming3.gamelibrary.repository.PlatformRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Profile("jpa")
@Repository
public class JPAPlatformRepository implements PlatformRepository {
    @PersistenceContext
    private EntityManager em;
    @Override
    public List<Platform> readPlatforms() {
        List<Platform> platforms = em.createQuery("select platform from Platform platform", Platform.class)
                .getResultList();
        return platforms;
    }

    @Override
    @Transactional
    public Platform createPlatform(Platform platform) {
        em.persist(platform);
        platform.getGames().forEach(game -> game.addPlatform(platform));
        return platform;
    }

    @Override
    public void updatePlatform(Platform platform) {
        em.merge(platform);
    }

    @Override
    @Transactional
    public void deletePlatform(int id) {
       Platform platform= em.find(Platform.class, id);
       platform.getGames().forEach(game -> game.getPlatforms().remove(platform));
        em.remove(em.find(Platform.class, id));
    }

    @Override
    public Platform findByName(String name) {
      return   em.find(Platform.class, name);
    }

    @Override
    public Platform findById(int id) {
       return em.find(Platform.class, id);
    }
}
