package be.kdg.programming3.gamelibrary.repository;


import be.kdg.programming3.gamelibrary.domain.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

public class DataFactory {
    public static List<Game> games;
    public static List<Platform> platforms;
    public static List<Publisher> publishers;

    static {
        seed();
    }

    public static void seed() {
        games = new ArrayList<>();
        platforms = new ArrayList<>();
        publishers = new ArrayList<>();


        Publisher mojang = new Publisher(1,"Mojang", "Stockholm, Sweden", LocalDate.of(2009, Month.OCTOBER, 3));
        Publisher bathesda = new Publisher(2,"Bathesda", "Rockville, Maryland, United States", LocalDate.of(1986, Month.JUNE, 28));

        Publisher rockstar = new Publisher(3,"Rockstar Games", "New York, New York, United States", LocalDate.of(1998, Month.DECEMBER, 7));
        Publisher nintendo = new Publisher(4,"Nintendo", " Kyoto, Japan", LocalDate.of(1889, Month.SEPTEMBER, 23));

        publishers.add(mojang);
        publishers.add(bathesda);
        publishers.add(nintendo);
        publishers.add(rockstar);



        Game minecraft = new Game(1,"Minecraft", mojang,Genre.SANDBOX, LocalDate.of(2011, Month.NOVEMBER, 18), 29.99, 238000000);
        Game eso = new Game(2,"The Elder Scrolls Online", bathesda,Genre.MMORPG, LocalDate.of(2014, Month.APRIL, 4), 19.99, 18000000);
        Game skyrim = new Game(3, "The Elder Scrolls V Skyrim",bathesda, Genre.RPG, LocalDate.of(2011, Month.NOVEMBER, 11), 39.99, 30000000);
        Game rdr = new Game(4,"Red Dead Redemption",rockstar, Genre.RPG, LocalDate.of(2010, Month.MAY, 21), 29.99, 23000000);
        Game pokemon_crystal = new Game(5,"Pokemon Crystal",nintendo ,Genre.RPG, LocalDate.of(2000, Month.DECEMBER, 14), 585, 6400000);

        games.add(minecraft);
        games.add(eso);
        games.add(skyrim);
        games.add(rdr);
        games.add(pokemon_crystal);


        Platform ps4 = new Platform(1,"PlayStation4", PlatformDevelopers.SONY, LocalDate.of(2013, Month.NOVEMBER, 15), 8);
        Platform ps5 = new Platform(2,"PlayStation5", PlatformDevelopers.SONY, LocalDate.of(2020, Month.NOVEMBER, 12), 9);
        Platform ps3 = new Platform(3,"PlayStation3", PlatformDevelopers.SONY, LocalDate.of(2006, Month.NOVEMBER, 11), 7);
        Platform xbox1 = new Platform(4,"XboxOne", PlatformDevelopers.MICROSOFT, LocalDate.of(2013, Month.NOVEMBER, 22), 8);
        Platform xboxSX = new Platform(5,"Xbox Series X/S", PlatformDevelopers.MICROSOFT, LocalDate.of(2020, Month.NOVEMBER, 10), 9);
        Platform xbox360 = new Platform(6,"Xbox360", PlatformDevelopers.MICROSOFT, LocalDate.of(2005, Month.NOVEMBER, 22), 7);
        Platform gameBoyColor = new Platform(7,"Game Boy Color", PlatformDevelopers.NINTENDO, LocalDate.of(1998, Month.OCTOBER, 21), 5);
        Platform nintendo3ds = new Platform(8,"Nintendo 3DS", PlatformDevelopers.NINTENDO, LocalDate.of(2011, Month.FEBRUARY, 26), 8);
        Platform windows7 = new Platform(9,"Windows 7", PlatformDevelopers.MICROSOFT, LocalDate.of(2009, Month.OCTOBER, 22), 6);
        Platform windows8 = new Platform(10,"Windows 8", PlatformDevelopers.MICROSOFT, LocalDate.of(2012, Month.AUGUST, 1), 7);
        Platform windows10 = new Platform(11,"Windows 10", PlatformDevelopers.MICROSOFT, LocalDate.of(2015, Month.JULY, 29), 8);
        Platform windows11 = new Platform(12,"Windows 11", PlatformDevelopers.MICROSOFT, LocalDate.of(2021, Month.OCTOBER, 5), 9);
        Platform stadia = new Platform(13,"Stadia", PlatformDevelopers.GOOGLE, LocalDate.of(2019, Month.NOVEMBER, 19), 9);


        platforms.add(ps4);
        platforms.add(ps5);
        platforms.add(ps3);
        platforms.add(xbox1);
        platforms.add(xbox360);
        platforms.add(xboxSX);
        platforms.add(gameBoyColor);
        platforms.add(nintendo3ds);
        platforms.add(windows7);
        platforms.add(windows8);
        platforms.add(windows10);
        platforms.add(windows11);
        platforms.add(stadia);

        minecraft.addPlatform(windows7);
        minecraft.addPlatform(windows8);
        minecraft.addPlatform(windows10);
        minecraft.addPlatform(windows11);
        minecraft.addPlatform(ps4);
        minecraft.addPlatform(ps3);
        minecraft.addPlatform(xbox1);
        minecraft.addPlatform(xbox360);
        minecraft.addPlatform(ps5);
        minecraft.addPlatform(xboxSX);


        eso.addPlatform(windows8);
        eso.addPlatform(windows10);
        eso.addPlatform(windows11);
        eso.addPlatform(ps4);
        eso.addPlatform(stadia);
        eso.addPlatform(xbox1);


        skyrim.addPlatform(windows7);
        skyrim.addPlatform(windows8);
        skyrim.addPlatform(windows10);
        skyrim.addPlatform(windows11);
        skyrim.addPlatform(ps4);
        skyrim.addPlatform(ps3);
        skyrim.addPlatform(xbox1);
        skyrim.addPlatform(xbox360);
        skyrim.addPlatform(ps5);
        skyrim.addPlatform(xboxSX);


        rdr.addPlatform(ps3);
        rdr.addPlatform(xbox360);


        pokemon_crystal.addPlatform(gameBoyColor);
        pokemon_crystal.addPlatform(nintendo3ds);


        ps5.addGame(minecraft);
        ps5.addGame(skyrim);
        ps5.addGame(eso);

        xboxSX.addGame(minecraft);
        xboxSX.addGame(skyrim);
        xboxSX.addGame(eso);

        ps4.addGame(minecraft);
        ps4.addGame(eso);
        ps4.addGame(skyrim);

        xbox1.addGame(minecraft);
        xbox1.addGame(eso);
        xbox1.addGame(skyrim);

        ps3.addGame(minecraft);
        ps3.addGame(skyrim);
        ps3.addGame(rdr);

        xbox360.addGame(minecraft);
        xbox360.addGame(skyrim);
        xbox360.addGame(rdr);

        stadia.addGame(eso);

        nintendo3ds.addGame(pokemon_crystal);
        gameBoyColor.addGame(pokemon_crystal);


        windows7.addGame(minecraft);
        windows7.addGame(skyrim);

        windows8.addGame(minecraft);
        windows8.addGame(skyrim);
        windows8.addGame(eso);

        windows10.addGame(minecraft);
        windows10.addGame(skyrim);
        windows10.addGame(eso);

        windows11.addGame(minecraft);
        windows11.addGame(skyrim);
        windows11.addGame(eso);







        mojang.addGame(minecraft);
        bathesda.addGame(eso);
        bathesda.addGame(skyrim);
        rockstar.addGame(rdr);
        nintendo.addGame(pokemon_crystal);


    }
}
