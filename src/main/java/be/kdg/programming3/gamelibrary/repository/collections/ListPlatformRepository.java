package be.kdg.programming3.gamelibrary.repository.collections;


import be.kdg.programming3.gamelibrary.domain.Game;
import be.kdg.programming3.gamelibrary.domain.Platform;
import be.kdg.programming3.gamelibrary.repository.DataFactory;
import be.kdg.programming3.gamelibrary.repository.PlatformRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Component
@Profile("collections")
public class ListPlatformRepository implements PlatformRepository {
    static List<Platform> platforms = DataFactory.platforms;


    @Override
    public List<Platform> readPlatforms() {
        return platforms;
    }

    @Override
    public Platform createPlatform(Platform platform) {
        platform.setId(platforms.size()+1);
        platforms.add(platform);
        return platform;

    }

    @Override
    public void updatePlatform(Platform platform) {

    }

    @Override
    public void deletePlatform(int id) {
        platforms.remove(findById(id));
    }


    @Override
    public Platform findByName(String name) {
        Optional<Platform> targetPlatform= readPlatforms().stream().filter(platform -> platform.getName().equals(name)).findFirst();
        return targetPlatform.get();
    }

    @Override
    public Platform findById(int id) {
        Optional<Platform> targetPlatform= readPlatforms().stream().filter(platform -> platform.getId()==id).findFirst();
        return targetPlatform.get();
    }
}
