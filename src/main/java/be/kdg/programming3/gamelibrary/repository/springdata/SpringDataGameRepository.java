package be.kdg.programming3.gamelibrary.repository.springdata;

import be.kdg.programming3.gamelibrary.domain.Game;
import be.kdg.programming3.gamelibrary.domain.Platform;
import be.kdg.programming3.gamelibrary.domain.Publisher;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

@Profile("springData")
public interface SpringDataGameRepository extends JpaRepository<Game, Integer> {
    List<Game> findByPlatforms_Id(int id);

    Optional<Game> findByName(String name);
    List<Game> findByPublisher(Publisher publisher);

    @Query(value = "SELECT pub FROM Publisher pub")
    List<Publisher> findAllPublishers();


}
