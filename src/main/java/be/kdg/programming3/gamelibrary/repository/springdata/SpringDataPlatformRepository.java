package be.kdg.programming3.gamelibrary.repository.springdata;

import be.kdg.programming3.gamelibrary.domain.Platform;
import be.kdg.programming3.gamelibrary.domain.PlatformDevelopers;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;

@Profile("springData")
public interface SpringDataPlatformRepository extends JpaRepository<Platform, Integer> {
    Optional<Platform> findByName(String name);
    List<Platform> findByGenerationAndDeveloper(int generation, PlatformDevelopers developer);


}
