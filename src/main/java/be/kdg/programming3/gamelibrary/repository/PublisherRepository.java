package be.kdg.programming3.gamelibrary.repository;

import be.kdg.programming3.gamelibrary.domain.Game;
import be.kdg.programming3.gamelibrary.domain.Publisher;

import java.util.List;

public interface PublisherRepository {
    List<Publisher> readPublishers();
    Publisher createPublishers(Publisher publisher);
    public void updatePublisher(Publisher publisher);

    public void deletePublisher(String name);
    Publisher findById(int id);
}
