package be.kdg.programming3.gamelibrary.repository;

import be.kdg.programming3.gamelibrary.domain.Game;
import be.kdg.programming3.gamelibrary.domain.Platform;

import java.util.List;

public interface PlatformRepository {

    List<Platform> readPlatforms();

    Platform createPlatform(Platform platform);
    public void updatePlatform(Platform platform);

    public void deletePlatform(int id);

    Platform findByName(String name);
    Platform findById(int id);
}
