package be.kdg.programming3.gamelibrary.repository.jdbc;

import be.kdg.programming3.gamelibrary.domain.Platform;
import be.kdg.programming3.gamelibrary.domain.PlatformDevelopers;
import be.kdg.programming3.gamelibrary.repository.PlatformRepository;
import org.springframework.context.annotation.Profile;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.simple.SimpleJdbcInsert;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;


@Repository
@Profile("jdbc")
public class H2PlatformRepository implements PlatformRepository
{

    private JdbcTemplate jdbcTemplate;
    private SimpleJdbcInsert inserter;

    public H2PlatformRepository(JdbcTemplate jdbcTemplate) {

        this.jdbcTemplate = jdbcTemplate;
        this.inserter=new SimpleJdbcInsert(jdbcTemplate).withTableName("PLATFORMS").
                usingGeneratedKeyColumns("ID");
    }


    public static Platform mapRowPlatform(ResultSet rs, int rowid) throws SQLException{
        String pDevStr=rs.getString("DEVELOPER");
        PlatformDevelopers developer = Arrays.stream(PlatformDevelopers.values()).filter(dev -> dev.toString().equals(pDevStr)).findFirst().get();
        return  new Platform(rs.getInt(1),rs.getString("NAME"), developer, rs.getDate("RELEASE_DATE").toLocalDate(), rs.getInt("GENERATION"));
    }





    @Override
    public List<Platform> readPlatforms() {
        return jdbcTemplate.query("SELECT * FROM PLATFORMS", H2PlatformRepository::mapRowPlatform);
    }

    @Override
    public Platform createPlatform(Platform platform) {
        Map<String,Object> parameters = new HashMap<>();
        parameters.put("NAME", platform.getName());
        parameters.put("DEVELOPER", platform.getDeveloper());
        parameters.put("RELEASE_DATE", platform.getReleaseDate());
        parameters.put("GENERATION", platform.getGeneration());
        platform.setId(inserter.executeAndReturnKey(parameters).intValue());
        if (!platform.getGames().isEmpty()){
            platform.getGames().forEach((game) -> jdbcTemplate.update("INSERT INTO GAME_PLATFORM (GAME_ID, PLATFORM_ID) VALUES ( ?,? )",game.getId(),platform.getId()));

        }
        return platform;
    }

    @Override
    public void updatePlatform(Platform platform) {
        jdbcTemplate.update("UPDATE PLATFORMS SET NAME=?, DEVELOPER=?,RELEASE_DATE=?, GENERATION=? WHERE ID=?",
              platform.getName(), platform.getDeveloper(), platform.getReleaseDate(), platform.getGeneration(), platform.getId());


    }


    @Override
    @Transactional
    public void deletePlatform(int id) {
        jdbcTemplate.update("DELETE FROM GAME_PLATFORM WHERE PLATFORM_ID=?", id);
        jdbcTemplate.update("DELETE FROM PLATFORMS WHERE ID=?", id);

    }

    @Override
    public Platform findByName(String name) {
        Optional<Platform> targetPlatform= readPlatforms().stream().filter(platform -> platform.getName().equals(name)).findFirst();
        return targetPlatform.get();
    }


    @Override
    public Platform findById(int id) {
        Optional<Platform> targetPlatform= readPlatforms().stream().filter(platform -> platform.getId()==id).findFirst();
        return targetPlatform.get();
    }
}
