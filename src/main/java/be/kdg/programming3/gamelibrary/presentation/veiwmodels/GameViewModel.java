package be.kdg.programming3.gamelibrary.presentation.veiwmodels;


import be.kdg.programming3.gamelibrary.domain.Genre;
import be.kdg.programming3.gamelibrary.domain.Platform;
import be.kdg.programming3.gamelibrary.domain.Publisher;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Component
@Validated
public class GameViewModel {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    @NotBlank(message = "name is mandatory")
    @Size(min=3, max=100, message = "Name should have length between 2 and 100")
    private String name;


    private Publisher publisher;


    private Genre genre;
    @NotNull(message = "release date is mandatory")
    private @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate releaseDate;


    @Min(value = 0, message = "Price must be positive")
    private double price;
    @Min(value = 0, message = "Copies sold must be positive")
    private int copiesSold;

   private List<Platform> platformList=new ArrayList<>();


    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher=publisher;

    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getCopiesSold() {
        return copiesSold;
    }

    public void setCopiesSold(int copiesSold) {
        this.copiesSold = copiesSold;
    }

    public List<Platform> getPlatformList() {
        return platformList;
    }

    public void setPlatformList(List<Platform> platformList) {
        this.platformList = platformList;
    }


//    public List<String> getPlatformList() {
//        return platformList;
//    }
//
//    public void setPlatformList(List<String> platformList) {
//        this.platformList = platformList;
//    }
}
