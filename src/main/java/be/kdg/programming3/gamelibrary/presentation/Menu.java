package be.kdg.programming3.gamelibrary.presentation;

import be.kdg.programming3.gamelibrary.domain.Game;
import be.kdg.programming3.gamelibrary.domain.Platform;
import be.kdg.programming3.gamelibrary.domain.PlatformDevelopers;
import be.kdg.programming3.gamelibrary.domain.Publisher;
import be.kdg.programming3.gamelibrary.repository.JSonWriter;
import be.kdg.programming3.gamelibrary.service.GameService;
import be.kdg.programming3.gamelibrary.service.PlatformService;
import org.springframework.stereotype.Component;


import java.util.List;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

@Component
public class Menu {

    private GameService gameService;
    private PlatformService platformService;

    public Menu(GameService gameService, PlatformService platformService) {
        this.gameService = gameService;
        this.platformService = platformService;
    }


    private Scanner scanner = new Scanner(System.in);
    private JSonWriter jSonWriter = new JSonWriter();

    public void show() {
        while (true) {
            System.out.println("What would you like to do?");
            System.out.println("==========================");
            System.out.println("0) Quit");
            System.out.println("1) Show all games");
            System.out.println("2) Show games from a certain publisher");
            System.out.println("3) Show all platforms");
            System.out.println("4) Show all platforms from a certain developer/minimal generation");
            System.out.print(" Choice (0-4):");
            int choice = scanner.nextInt();
            switch (choice) {
                case 0 -> {
                    System.out.println("Bye...");
                    System.exit(0);
                }
                case 1 -> showAllGames();
                case 2 -> showGamesFromPublisher();
                case 3 -> showAllPlatforms();
                case 4 -> showPlatformsWithDevAndGen();
                default -> System.out.println("Invalid choice...");
            }
        }
    }

    private void showAllGames() {
        System.out.println("All games:");
        System.out.println("=================");
        gameService.getAllGames().forEach(System.out::println);
        System.out.println("Writing to json...");
        gameService.writeGameToJson(gameService.getAllGames());
    }

    private void showGamesFromPublisher() {
        System.out.println("Choose publisher:");

//        List<Publisher> publishers = DataFactory.games.stream().map(Game::getPublisher).distinct().toList();
        AtomicInteger i = new AtomicInteger(1);
//        publishers.forEach(telescope -> {
////            System.out.println(i.getAndIncrement() + ") " + telescope.toString());
////        });
//

        List<Publisher> publishers = gameService.getPublishers();

        publishers.forEach(telescope -> {
            System.out.println(i.getAndIncrement() + ") " + telescope.toString());
        });


        int choice = scanner.nextInt();
        if (choice <= 0 || choice > publishers.size()) {
            System.out.println("Invalid choice...");
        } else {
            System.out.println("Games by " + publishers.get(choice - 1));
            List<Game> selection = gameService.getGameFromPublisher(publishers.get(choice - 1));
            selection.forEach(System.out::println);
            System.out.println("Writing to json...");
            gameService.writeGameToJson(selection);


        }
    }

    private void showAllPlatforms() {
        System.out.println("All platforms:");
        System.out.println("=================");
        platformService.getPlatforms().forEach(System.out::println);
        System.out.println("Writing to json...");
        platformService.writePlatformToJson(platformService.getPlatforms());
    }

    private void showPlatformsWithDevAndGen() {
        scanner.nextLine();
        System.out.println("Enter generation (can be left empty):");
        String input = scanner.nextLine();
        List<Platform> objects = platformService.getPlatforms();
        int generation = -1;
        if (input.length() != 0) {
            generation = Integer.parseInt(input);

        }
        System.out.println("Choose developer (can be left empty):");
        AtomicInteger i = new AtomicInteger(1);
        Stream.of(PlatformDevelopers.values()).forEach(objecttype -> {
            System.out.println(i.getAndIncrement() + ") " + objecttype.toString());
        });
        input = scanner.nextLine();
        if (input.length() != 0) {
            int choice = Integer.parseInt(input);
            if (choice <= 0 || choice > PlatformDevelopers.values().length) {
                System.out.println("Invalid choice");
            } else {
                objects = platformService.getPlatformsWithGenAndDev(generation, PlatformDevelopers.values()[choice - 1]);
            }
        }
        objects.forEach(System.out::println);
        System.out.println("Writing to json...");
        jSonWriter.writePlatform(objects);
    }
}
