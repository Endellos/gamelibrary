package be.kdg.programming3.gamelibrary.presentation.controllers.api.dto;

import be.kdg.programming3.gamelibrary.domain.PlatformDevelopers;

public class PlatformDto {

    private  String name;
    private PlatformDevelopers developer;
    private  int generation;

    public PlatformDto(String name, PlatformDevelopers developer, int generation) {
        this.name = name;
        this.developer = developer;
        this.generation = generation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PlatformDevelopers getDeveloper() {
        return developer;
    }

    public void setDeveloper(PlatformDevelopers developer) {
        this.developer = developer;
    }

    public int getGeneration() {
        return generation;
    }

    public void setGeneration(int generation) {
        this.generation = generation;
    }
}
