package be.kdg.programming3.gamelibrary.presentation.coverters.modelMapper;


import be.kdg.programming3.gamelibrary.domain.Platform;
import be.kdg.programming3.gamelibrary.service.PlatformService;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;

import java.util.ArrayList;
import java.util.List;

public class PlatformNameToPlatformConverter implements Converter<List<String>, List<Platform>> {
    private final PlatformService platformService;

    public PlatformNameToPlatformConverter(PlatformService platformService) {
        this.platformService = platformService;
    }


    @Override
    public List<Platform> convert(MappingContext<List<String>, List<Platform>> context) {
        List<String> platformNames = context.getSource();
        List<Platform> platforms = new ArrayList<>();
        for (String platformName : platformNames) {
            platforms.add( platformService.findByName(platformName));
        }
        return platforms;

    }
}
