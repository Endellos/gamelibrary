package be.kdg.programming3.gamelibrary.presentation.controllers.api;

import be.kdg.programming3.gamelibrary.domain.Game;
import be.kdg.programming3.gamelibrary.domain.Platform;
import be.kdg.programming3.gamelibrary.domain.Publisher;
import be.kdg.programming3.gamelibrary.presentation.controllers.api.dto.GameDto;
import be.kdg.programming3.gamelibrary.presentation.controllers.api.dto.NewGameDto;
import be.kdg.programming3.gamelibrary.presentation.controllers.api.dto.PlatformDto;
import be.kdg.programming3.gamelibrary.presentation.coverters.modelMapper.PlatformNameToPlatformConverter;
import be.kdg.programming3.gamelibrary.presentation.coverters.modelMapper.PublisherIdToPublisherConverter;
import be.kdg.programming3.gamelibrary.service.GameService;
import be.kdg.programming3.gamelibrary.service.PlatformService;
import be.kdg.programming3.gamelibrary.service.PublisherService;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api/games")
public class GamesApiController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private final GameService gameService;
    private final PlatformService platformService;
    private final PublisherService publisherService;

    public GamesApiController(GameService gameService, PlatformService platformService, PublisherService publisherService) {
        this.gameService = gameService;
        this.platformService = platformService;
        this.publisherService = publisherService;
    }




    @GetMapping("/{id}")
    public ResponseEntity<GameDto> getGameWithPlatforms(
            @PathVariable("id") int gameId
    ) {
        var game = gameService.findById(gameId);
        if (game == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {

            return new ResponseEntity<>(
                    new GameDto(game.getName(), game.getPlatforms().stream().map(platform -> new PlatformDto(platform.getName(), platform.getDeveloper(), platform.getGeneration())).toList()
                    ), HttpStatus.OK);

        }


    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteGame(@PathVariable int id) {
        // TODO: STATUS CODES !!!!
        // TODO: more is needed here!!!

        gameService.deleteGame(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PostMapping("add")
    public ResponseEntity<GameDto> addGame(
            @RequestBody @Valid NewGameDto newGameDto
    ) {






        Game createdGame = new Game(newGameDto.getName(), newGameDto.getGenre(), newGameDto.getReleaseDate(), newGameDto.getPrice(),newGameDto.getCopiesSold());

        // Find publisher by ID and set as publisher of the new game
        Publisher publisher = publisherService.findById(newGameDto.getPublisherId());
        if (publisher == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        createdGame.setPublisher(publisher);
        List<Platform> platforms = new ArrayList<>();
        for (String platformName: newGameDto.getPlatformList()) {
            Platform platform = platformService.findByName(platformName);
            platforms.add(platform);
        }

       createdGame.setPlatforms(platforms);

        gameService.addGame(createdGame);


        return new ResponseEntity<>(
                new GameDto(
                        createdGame.getName(),
                        createdGame.getPublisher(),
                        createdGame.getGenre(),
                        createdGame.getReleaseDate(),
                        createdGame.getPrice(),
                        createdGame.getCopiesSold()),

                HttpStatus.CREATED);
    }


//    @PatchMapping("/games/{id}")
//    public ResponseEntity<Void> updatePlatformsList(@PathVariable int id){
//
//    }
}
