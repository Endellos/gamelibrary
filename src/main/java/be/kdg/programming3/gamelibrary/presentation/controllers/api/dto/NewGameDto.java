package be.kdg.programming3.gamelibrary.presentation.controllers.api.dto;

import be.kdg.programming3.gamelibrary.domain.Genre;
import be.kdg.programming3.gamelibrary.domain.Platform;
import be.kdg.programming3.gamelibrary.domain.Publisher;
import be.kdg.programming3.gamelibrary.service.PublisherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Valid
public class NewGameDto {
    private Logger logger = LoggerFactory.getLogger(this.getClass());


    @NotBlank(message = "name is mandatory")
    @Size(min=3, max=100, message = "Name should have length between 2 and 100")
    private String name;





    private Genre genre;
    @NotNull(message = "release date is mandatory")
    private @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate releaseDate;


    @Min(value = 0, message = "Price must be positive")
    private double price;
    @Min(value = 0, message = "Copies sold must be positive")
    private int copiesSold;

    private List<String> platformList=new ArrayList<>();

private int publisherId;


    public NewGameDto(String name, int publisherId, Genre genre, LocalDate releaseDate, double price, int copiesSold, List<String> platformList) {
        this.name = name;
        this.genre = genre;
        this.releaseDate = releaseDate;
        this.price = price;
        this.copiesSold = copiesSold;
        this.platformList = platformList;
        this.publisherId = publisherId;
    }

    public NewGameDto() {
    }

    public NewGameDto(String name, Genre genre, LocalDate releaseDate, double price, int copiesSold, List<String> platformList) {
        this.name = name;
        this.genre = genre;
        this.releaseDate = releaseDate;
        this.price = price;
        this.copiesSold = copiesSold;
        this.platformList = platformList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }



    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getCopiesSold() {
        return copiesSold;
    }

    public void setCopiesSold(int copiesSold) {
        this.copiesSold = copiesSold;
    }

    public List<String> getPlatformList() {
        return platformList;
    }

    public void setPlatformList(List<String> platformList) {
        this.platformList = platformList;
    }

    public int getPublisherId() {
        return publisherId;
    }

    public void setPublisherId(int publisherId) {
        this.publisherId = publisherId;
    }
}
