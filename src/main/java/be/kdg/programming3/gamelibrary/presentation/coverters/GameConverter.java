package be.kdg.programming3.gamelibrary.presentation.coverters;

import be.kdg.programming3.gamelibrary.domain.Game;
import be.kdg.programming3.gamelibrary.repository.GameRepository;
import be.kdg.programming3.gamelibrary.service.GameService;
import org.springframework.core.convert.converter.Converter;

public class GameConverter implements Converter<String, Game> {

   GameService gameService;

    public GameConverter(GameService gameService) {
        this.gameService = gameService;
    }

    @Override
    public Game convert(String source) {

       return gameService.getAllGames().stream().filter(game -> game.getName().equals(source)).findFirst().get();

    }
}
