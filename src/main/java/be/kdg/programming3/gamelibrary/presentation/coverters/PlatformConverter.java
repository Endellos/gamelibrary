package be.kdg.programming3.gamelibrary.presentation.coverters;

import be.kdg.programming3.gamelibrary.domain.Platform;
import be.kdg.programming3.gamelibrary.domain.Publisher;
import be.kdg.programming3.gamelibrary.repository.PlatformRepository;
import be.kdg.programming3.gamelibrary.service.PlatformService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public class PlatformConverter implements Converter<String , Platform> {

    PlatformService platformService;

    public PlatformConverter(PlatformService platformService) {
        this.platformService = platformService;
    }

    @Override
    public Platform convert(String source) {
  return platformService.getPlatforms().stream().filter(platform -> platform.getName().equals(source)).findFirst().get();

    }
}
