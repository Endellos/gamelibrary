package be.kdg.programming3.gamelibrary.presentation.veiwmodels;

import be.kdg.programming3.gamelibrary.domain.Game;
import be.kdg.programming3.gamelibrary.domain.PlatformDevelopers;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Validated
public class PlatformViewModel {

    @NotBlank(message = "name is mandatory")
    @Size(min=3, max=100, message = "Name should have length between 2 and 100")
    private  String name;
    private PlatformDevelopers developer;

    @NotNull(message = "release date is mandatory")
    private @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate releaseDate;
    @Min(value = 1, message = "Generation must be positive")
    private  int generation;

    private List<Game> gamesList = new ArrayList<>();

    public List<Game> getGamesList() {
        return gamesList;
    }

    public void setGamesList(List<Game> gamesList) {
        this.gamesList = gamesList;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public PlatformDevelopers getDeveloper() {
        return developer;
    }

    public void setDeveloper(PlatformDevelopers developer) {
        this.developer = developer;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public int getGeneration() {
        return generation;
    }

    public void setGeneration(int generation) {
        this.generation = generation;
    }
}
