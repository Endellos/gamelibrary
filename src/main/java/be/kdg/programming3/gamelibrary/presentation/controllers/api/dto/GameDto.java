package be.kdg.programming3.gamelibrary.presentation.controllers.api.dto;

import be.kdg.programming3.gamelibrary.domain.Genre;
import be.kdg.programming3.gamelibrary.domain.Platform;
import be.kdg.programming3.gamelibrary.domain.Publisher;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.Min;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class GameDto {


    public GameDto(String name, List<PlatformDto> platformList) {
        this.name = name;
        this.platformList = platformList;
    }

    public GameDto(String name, Publisher publisher, Genre genre, LocalDate releaseDate, double price, int copiesSold) {
        this.name = name;
        this.publisher = publisher;
        this.genre = genre;
        this.releaseDate = releaseDate;
        this.price = price;
        this.copiesSold = copiesSold;

    }

    public GameDto(String name, Publisher publisher, Genre genre, LocalDate releaseDate, double price, int copiesSold, List<PlatformDto> platformList) {
        this.name = name;
        this.publisher = publisher;
        this.genre = genre;
        this.releaseDate = releaseDate;
        this.price = price;
        this.copiesSold = copiesSold;
        this.platformList = platformList;
    }

    private String name;


    private Publisher publisher;


    private Genre genre;

    private @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate releaseDate;



    private double price;

    private int copiesSold;
    List<PlatformDto> platformList = new ArrayList<>();


        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

    public List<PlatformDto> getPlatformList() {
        return platformList;
    }

    public void setPlatformList(List<PlatformDto> platformList) {
        this.platformList = platformList;
    }

    public Publisher getPublisher() {
        return publisher;
    }

    public void setPublisher(Publisher publisher) {
        this.publisher = publisher;
    }

    public Genre getGenre() {
        return genre;
    }

    public void setGenre(Genre genre) {
        this.genre = genre;
    }

    public LocalDate getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(LocalDate releaseDate) {
        this.releaseDate = releaseDate;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getCopiesSold() {
        return copiesSold;
    }

    public void setCopiesSold(int copiesSold) {
        this.copiesSold = copiesSold;
    }
}


