package be.kdg.programming3.gamelibrary.presentation.controllers.mvc;

import be.kdg.programming3.gamelibrary.domain.Game;
import be.kdg.programming3.gamelibrary.domain.Genre;
import be.kdg.programming3.gamelibrary.domain.Publisher;
import be.kdg.programming3.gamelibrary.exceptions.ObjectDoesNotExistException;
import be.kdg.programming3.gamelibrary.presentation.veiwmodels.GameViewModel;
import be.kdg.programming3.gamelibrary.presentation.veiwmodels.PublisherViewModel;
import be.kdg.programming3.gamelibrary.service.PlatformService;
import be.kdg.programming3.gamelibrary.service.PublisherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.LocalDateTime;
import java.util.List;

@Controller
@RequestMapping("/publishers")
public class PublisherController {

    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private PublisherService publisherService;

    public PublisherController(PublisherService publisherService) {
        this.publisherService = publisherService;
    }

    @GetMapping
    public String showPublishersView(Model model, HttpSession httpSession) {
        logger.info("controller is running showPublishersView!");
        List<Publisher> publishers = publisherService.getPublishers();

        model.addAttribute("publishers", publishers);


        String url = " /publishers";
        String timeStamp = LocalDateTime.now().toString();
        httpSession.setAttribute(timeStamp, url);


        return "allpublishers";
    }
    @GetMapping("/add")
    public String showAddPublisherForm(Model model, HttpSession httpSession) {


        model.addAttribute("publisher", new PublisherViewModel());
        logger.info("Add publisher shown");


        String url = " /publishers/add";

        String timeStamp = LocalDateTime.now().toString();
        httpSession.setAttribute(timeStamp, url);


        return "addpublisher";
    }

    @PostMapping("/add")
    public String processAddPublisherForm(@Valid @ModelAttribute("publisher") PublisherViewModel viewModel, BindingResult errors) {

        logger.info("Processing " + viewModel);

        if (errors.hasErrors()) {
            errors.getAllErrors().forEach(error -> {
                logger.error(error.toString());
            });

            return "addpublisher";

        }





        publisherService.addPublisher(viewModel.getName(),viewModel.getHeadquarters(),viewModel.getFounded());

        logger.info(viewModel.getName() + " added");

        return "redirect:/publishers";


    }

    @GetMapping(value = "/{id}")
    public String showIndividualGame(@PathVariable("id")int id, Model model, HttpSession httpSession) {
        try {
            publisherService.findById(id);
        } catch (ObjectDoesNotExistException e) {
            logger.error("Error: " + e.getMessage());

        }
        Publisher indvPublisher = publisherService.findById(id);
        model.addAttribute("indvPublisher", indvPublisher);


        String url = " /publishers/" + id;
        String timeStamp = LocalDateTime.now().toString();


        httpSession.setAttribute(timeStamp, url);


        return "onepublisher";
    }
    @ExceptionHandler(ObjectDoesNotExistException.class)
    public String handelObjectDoesNotExistException(Exception e, Model model) {
        logger.error(e.getMessage());
        model.addAttribute("error", "Not Found");
        model.addAttribute("status", 404);
        model.addAttribute("message", "Publisher does not exist");
        return "error";
    }

}





