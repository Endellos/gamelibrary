package be.kdg.programming3.gamelibrary.presentation.controllers.mvc;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;
import java.time.LocalDateTime;

@Controller
@RequestMapping
public class HomeController {

    @GetMapping(value={"", "/"})
   public String showHomeView(HttpSession httpSession){
        String url = "/";

        String timeStamp = LocalDateTime.now().toString();
        httpSession.setAttribute(timeStamp, url);

    return "home";
   }
}
