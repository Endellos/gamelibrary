package be.kdg.programming3.gamelibrary.presentation.controllers.api;

import be.kdg.programming3.gamelibrary.domain.Publisher;
import be.kdg.programming3.gamelibrary.presentation.controllers.api.dto.GameDto;
import be.kdg.programming3.gamelibrary.presentation.controllers.api.dto.PlatformDto;
import be.kdg.programming3.gamelibrary.presentation.controllers.api.dto.PublisherDto;
import be.kdg.programming3.gamelibrary.service.PublisherService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/publishers")
public class PublisherApiController {
    private final PublisherService publisherService;

    @Autowired
    public PublisherApiController(PublisherService publisherService) {
        this.publisherService = publisherService;
    }

    @GetMapping("/{id}")
    public ResponseEntity<PublisherDto> getGameWithPlatforms(
            @PathVariable("id") int publisherId
    ) {
        var publisher = publisherService.findById(publisherId);
        if (publisher == null) {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        } else {

            return new ResponseEntity<>(
                    new PublisherDto(publisher.getPublisherName(),publisher.getHeadquarters(),publisher.getFounded()), HttpStatus.OK);

        }


    }


    @PutMapping("/{id}")
    public ResponseEntity<PublisherDto> updatePublisher( @PathVariable("id") int publisherId, @RequestBody PublisherDto publisherDetails){
        publisherService.updatePublisher(publisherId, publisherDetails.getName(),publisherDetails.getHeadquarters(),publisherDetails.getFounded());

    return new ResponseEntity<>( new PublisherDto(publisherDetails.getName(),publisherDetails.getHeadquarters(),publisherDetails.getFounded()),HttpStatus.OK);
    }
}
