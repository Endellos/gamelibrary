package be.kdg.programming3.gamelibrary.presentation.coverters.modelMapper;

import be.kdg.programming3.gamelibrary.domain.Publisher;
import be.kdg.programming3.gamelibrary.service.PublisherService;
import org.modelmapper.Converter;
import org.modelmapper.spi.MappingContext;
import org.springframework.stereotype.Component;

@Component
public class PublisherIdToPublisherConverter implements Converter<Integer, Publisher> {
    private final PublisherService publisherService;

    public PublisherIdToPublisherConverter(PublisherService publisherService) {
        this.publisherService = publisherService;
    }

    @Override
    public Publisher convert(MappingContext<Integer, Publisher> context) {
        return publisherService.findById(context.getSource());
    }
}
