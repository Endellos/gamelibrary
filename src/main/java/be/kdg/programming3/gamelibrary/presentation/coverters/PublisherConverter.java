package be.kdg.programming3.gamelibrary.presentation.coverters;

import be.kdg.programming3.gamelibrary.domain.Publisher;
import be.kdg.programming3.gamelibrary.repository.*;
import be.kdg.programming3.gamelibrary.service.GameService;
import be.kdg.programming3.gamelibrary.service.GameServiceImp;
import be.kdg.programming3.gamelibrary.service.PublisherService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.convert.converter.Converter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import java.util.Optional;


@Component
public class PublisherConverter implements Converter<String , Publisher> {

    PublisherService publisherService;

    public PublisherConverter(PublisherService publisherService) {
        this.publisherService = publisherService;
    }

    @Override
    public Publisher convert(String source) {
        Publisher publisher = publisherService.getPublishers().stream().filter(publisher1 -> publisher1.getPublisherName().equals(source)).findFirst().get();
       return publisher;

    }


}
