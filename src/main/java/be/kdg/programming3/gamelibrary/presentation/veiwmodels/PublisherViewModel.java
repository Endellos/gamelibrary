package be.kdg.programming3.gamelibrary.presentation.veiwmodels;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Component
@Validated
public class PublisherViewModel {
    private Logger logger = LoggerFactory.getLogger(this.getClass());


    @Size(min=3, max=100, message = "Name should have length between 2 and 100")
    private String name;

    private String headquarters;

    @NotNull(message = "Founding  date is mandatory")
    @DateTimeFormat(pattern = "yyyy-MM-dd") private LocalDate founded;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHeadquarters() {
        return headquarters;
    }

    public void setHeadquarters(String headquarters) {
        this.headquarters = headquarters;
    }

    public LocalDate getFounded() {
        return founded;
    }

    public void setFounded(LocalDate founded) {
        this.founded = founded;
    }
}
