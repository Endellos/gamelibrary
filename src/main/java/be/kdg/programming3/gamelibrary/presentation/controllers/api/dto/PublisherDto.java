package be.kdg.programming3.gamelibrary.presentation.controllers.api.dto;

import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.validation.annotation.Validated;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.time.LocalDate;

@Validated
public class PublisherDto {
    @Size(min=3, max=100, message = "Name should have length between 2 and 100")
    private String name;

    private String headquarters;

    @NotNull(message = "Founding  date is mandatory")
    @DateTimeFormat(pattern = "yyyy-MM-dd") private LocalDate founded;


    public PublisherDto(String name, String headquarters, LocalDate founded) {
        this.name = name;
        this.headquarters = headquarters;
        this.founded = founded;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getHeadquarters() {
        return headquarters;
    }

    public void setHeadquarters(String headquarters) {
        this.headquarters = headquarters;
    }

    public LocalDate getFounded() {
        return founded;
    }

    public void setFounded(LocalDate founded) {
        this.founded = founded;
    }
}
