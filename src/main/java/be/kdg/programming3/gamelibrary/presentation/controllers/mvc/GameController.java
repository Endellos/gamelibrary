package be.kdg.programming3.gamelibrary.presentation.controllers.mvc;

import be.kdg.programming3.gamelibrary.domain.*;
import be.kdg.programming3.gamelibrary.exceptions.ObjectDoesNotExistException;
import be.kdg.programming3.gamelibrary.presentation.veiwmodels.GameViewModel;

import be.kdg.programming3.gamelibrary.service.GameService;
import be.kdg.programming3.gamelibrary.service.PlatformService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.LocalDateTime;

import java.util.List;
import java.util.NoSuchElementException;


@Controller
@RequestMapping("/games")
public class GameController {
    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private GameService gameService;
    private PlatformService platformService;

@Autowired
    public GameController(GameService gameService, PlatformService platformService) {
        this.gameService = gameService;
        this.platformService = platformService;
    }




    @GetMapping
    public String showGamesView(Model model, HttpSession httpSession) {
        logger.info("controller is running showGamesView!");
        List<Game> games = gameService.getAllGames();

        model.addAttribute("games", games);


        String url = " /games";
        String timeStamp = LocalDateTime.now().toString();
        httpSession.setAttribute(timeStamp, url);


        return "allgames";
    }

    @PostMapping
    public String deleteGame(@RequestParam("id") int id){

    gameService.deleteGame(id);

    return "redirect:/games";
    }

    @GetMapping("/add")
    public String showAddGameForm(Model model
//            ,
//                                  HttpSession httpSession
    ) {
        List<Publisher> publishersList = gameService.getPublishers();
        model.addAttribute("genres", Genre.values());
        model.addAttribute("publishersList", publishersList);
        model.addAttribute("game", new GameViewModel());
        model.addAttribute("platforms", platformService.getPlatforms());
        logger.info("Add game shown");


//        String url = " /games/ add";

        String timeStamp = LocalDateTime.now().toString();
//        httpSession.setAttribute(timeStamp, url);


        return "addgame";
    }


    @PostMapping("/add")
    public String processAddGameForm(){
        return "redirect:/games";


    }
    @ExceptionHandler(ObjectDoesNotExistException.class)
    public String handelObjectDoesNotExistException(Exception e, Model model){
        logger.error(e.getMessage());
        model.addAttribute("error", "Not Found");
        model.addAttribute("status", 404);
        model.addAttribute("message", "Game does not exist");
        return "error";
    }

    @GetMapping(value = "/{id}")
    public String showIndividualGame(@PathVariable("id") int id, Model model, HttpSession httpSession) {
    try {
        gameService.findById(id);
    }
    catch (ObjectDoesNotExistException e){
        logger.error("Error: "+e.getMessage());

    }
        Game indvGame = gameService.findById(id);

        model.addAttribute("indvGame", indvGame);


        String url = " /games/ " + id;
        String timeStamp = LocalDateTime.now().toString();
        logger.info("Platforms: "+indvGame.getPlatforms());

        httpSession.setAttribute(timeStamp, url);


        return "onegame";
    }

}
