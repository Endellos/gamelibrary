package be.kdg.programming3.gamelibrary.presentation.controllers.mvc;

import be.kdg.programming3.gamelibrary.domain.*;
import be.kdg.programming3.gamelibrary.exceptions.ObjectDoesNotExistException;
import be.kdg.programming3.gamelibrary.presentation.veiwmodels.PlatformViewModel;
import be.kdg.programming3.gamelibrary.service.GameService;
import be.kdg.programming3.gamelibrary.service.PlatformService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Controller
@RequestMapping("/platforms")
public class PlatformController {


    private Logger logger = LoggerFactory.getLogger(this.getClass());

    private PlatformService platformService;
    private GameService gameService;


    public PlatformController(PlatformService platformService, GameService gameService) {
        this.platformService = platformService;
        this.gameService = gameService;
    }

    @GetMapping
    public String showGamesView(Model model, HttpSession httpSession) {
        logger.info("controller is running showPlatformsView!");
        List<Platform> platforms = platformService.getPlatforms();

        model.addAttribute("platforms", platforms);

        String url = " /platforms";
        String timeStamp = LocalDateTime.now().toString();
        httpSession.setAttribute(timeStamp, url);


        return "allplatforms";
    }

    @PostMapping
    public String deletePlatform(@RequestParam("id") int id) {

        platformService.deletePlatform(id);

        return "redirect:/platforms";
    }


    @GetMapping("/add")
    public String showAddPlatformForm(Model model, HttpSession httpSession) {

        model.addAttribute("developers", PlatformDevelopers.values());
        model.addAttribute("platform", new PlatformViewModel());
        model.addAttribute("games", gameService.getAllGames());
        logger.info("Add platform shown");

        String url = " /platforms/add";
        String timeStamp = LocalDateTime.now().toString();
        httpSession.setAttribute(timeStamp, url);

        return "addplatform";
    }


    @PostMapping("/add")
    public String processAddPlatformForm(@Valid @ModelAttribute("platform")PlatformViewModel platformViewModel, BindingResult errors, Model model) {

        if (errors.hasErrors()) {
            errors.getAllErrors().forEach(error -> {
                logger.error(error.toString());
            });
            model.addAttribute("developers", PlatformDevelopers.values());
            model.addAttribute("platform", new PlatformViewModel());
            model.addAttribute("games", gameService.getAllGames());
            return "addplatform";

        }


        Platform platform = new Platform(platformViewModel.getName(), platformViewModel.getDeveloper(), platformViewModel.getReleaseDate(), platformViewModel.getGeneration());
        platform.setGames(platformViewModel.getGamesList());
        platformService.addPlatform(platform);
        logger.info("Games: " + platform.getGames());

        logger.info("Platform " + platform.getName() + " added");
        return "redirect:/platforms";


    }

    @GetMapping(value = "/{id}")
    public String showIndividualPlatform(@PathVariable("id") int id, Model model, HttpSession httpSession) {

        try {
            platformService.findById(id);
        } catch (ObjectDoesNotExistException e) {
            logger.error("Error: " + e.getMessage());

        }
        Platform indvPlatform = platformService.findById(id);
        model.addAttribute("indvPlatform", indvPlatform);
        model.addAttribute("gameList", gameService.findByPlatform(indvPlatform.getId()));

        String url = " /platforms/" + id;
        String timeStamp = LocalDateTime.now().toString();
        httpSession.setAttribute(timeStamp, url);
        return "oneplatform";
    }

    @ExceptionHandler(ObjectDoesNotExistException.class)
    public String handelObjectDoesNotExistException(Exception e, Model model) {
        logger.error(e.getMessage());
        model.addAttribute("error", "Not Found");
        model.addAttribute("status", 404);
        model.addAttribute("message", "Platform does not exist");
        return "error";
    }
}
