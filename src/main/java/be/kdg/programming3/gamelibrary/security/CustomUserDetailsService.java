package be.kdg.programming3.gamelibrary.security;

import be.kdg.programming3.gamelibrary.service.springdata.SpringDataUserService;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
public class CustomUserDetailsService implements UserDetailsService {
    private final SpringDataUserService userService;

    public CustomUserDetailsService(SpringDataUserService userService) {
        this.userService = userService;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        var user = userService.getUserByName(username);
        if (user != null) {
            var authorities = new ArrayList<SimpleGrantedAuthority>();
            authorities.add(new SimpleGrantedAuthority(user.getRole().getCode()));
            return new CustomUserDetails(user.getUsername(), user.getPassword(),
                    authorities, user.getId());
        }
        throw new UsernameNotFoundException("User '" + username + "' doesn't exist");
    }
}
