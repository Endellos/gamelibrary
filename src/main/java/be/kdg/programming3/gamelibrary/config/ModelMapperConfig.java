package be.kdg.programming3.gamelibrary.config;

import be.kdg.programming3.gamelibrary.domain.Game;
import be.kdg.programming3.gamelibrary.domain.Publisher;
import be.kdg.programming3.gamelibrary.presentation.controllers.api.dto.NewGameDto;
import be.kdg.programming3.gamelibrary.presentation.coverters.modelMapper.PlatformNameToPlatformConverter;
import be.kdg.programming3.gamelibrary.presentation.coverters.modelMapper.PublisherIdToPublisherConverter;
import be.kdg.programming3.gamelibrary.service.PlatformService;
import be.kdg.programming3.gamelibrary.service.PublisherService;
import org.modelmapper.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
//
//@Configuration
//public class ModelMapperConfig {
//
//    @Autowired
//    private PublisherService publisherService;
//    @Autowired
//    private PlatformService platformService;
//
//
//
//    @Bean
//    public ModelMapper modelMapper() {
//        ModelMapper modelMapper = new ModelMapper();
//
//        Converter<Integer, Publisher> publisherConverter = new PublisherIdToPublisherConverter(publisherService);
//        var platformConverter = new PlatformNameToPlatformConverter(platformService);
//
//
//        modelMapper.createTypeMap(NewGameDto.class, Game.class)
//                .addMappings(mapping -> mapping.using(publisherConverter).map(NewGameDto::getPublisherId, Game::setPublisher));
//
//        modelMapper.createTypeMap(NewGameDto.class, Game.class)
//                .addMappings(mapping -> mapping.using(platformConverter).map(NewGameDto::getPlatformList, Game::setPlatforms));
//
//
//        return modelMapper;
//    }
//}
