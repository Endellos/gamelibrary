package be.kdg.programming3.gamelibrary.config;

import be.kdg.programming3.gamelibrary.presentation.coverters.GameConverter;
import be.kdg.programming3.gamelibrary.presentation.coverters.PlatformConverter;
import be.kdg.programming3.gamelibrary.presentation.coverters.PublisherConverter;
import be.kdg.programming3.gamelibrary.service.GameService;
import be.kdg.programming3.gamelibrary.service.PlatformService;
import be.kdg.programming3.gamelibrary.service.PublisherService;
import org.springframework.context.annotation.Configuration;
import org.springframework.format.FormatterRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration

public class WebConfig implements WebMvcConfigurer {
   private PublisherService publisherService;
   private GameService gameService;
   private PlatformService platformService;

    public WebConfig(PublisherService publisherService, GameService gameService, PlatformService platformService) {
        this.publisherService = publisherService;
        this.gameService = gameService;
        this.platformService = platformService;
    }

    @Override
    public void addFormatters(FormatterRegistry registry){
        registry.addConverter(new PublisherConverter(publisherService));

        registry.addConverter(new PlatformConverter(platformService));
        registry.addConverter(new GameConverter(gameService));
    }
}
