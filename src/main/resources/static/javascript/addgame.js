/**
 * @type {HTMLFormElement}
 */
const form = document.getElementById("addGameForm");
const gameName = document.getElementById("name");
const gameGenre = document.getElementById("genre");

const gameReleaseDate = document.getElementById("releaseDate");
const gamePrice = document.getElementById("price");
const publisherSelect = document.getElementById("publisher");
const gameCopiesSold = document.getElementById("copiesSold");
const gamePlatforms = document.querySelectorAll(".platforms");
const submitButton = document.querySelector("#addGameForm > button");


submitButton.addEventListener("click", trySubmitForm);

function trySubmitForm(event) {

    const header = document.querySelector('meta[name="_csrf_header"]').content;
    const token = document.querySelector('meta[name="_csrf"]').content;
    const formIsValid = form.checkValidity();

    const selected = [];

    for (let i = 0; i < gamePlatforms.length; i++) {
        if (gamePlatforms[i].checked) {
            selected.push(gamePlatforms[i].value);
        }
    }
    console.log(selected)
    console.log(publisherSelect.value)


        // https://getbootstrap.com/docs/5.0/forms/validation/#how-it-works
    form.classList.add('was-validated');
    if (formIsValid){
        fetch('/api/games/add', {
            method: "POST",
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                    [header] : token
            },
            body:     JSON.stringify({
                "name": gameName.value,
                "publisherId":publisherSelect.value,
                "genre": gameGenre.value,
                "releaseDate": gameReleaseDate.value,
                "price": gamePrice.value,
                "copiesSold": gameCopiesSold.value,
                "platformList" : selected
            })
        })
            .then(response => response.json())
            .then(data => console.log(data))
            .catch(error => console.error(error));


}}
//
//
// function getPublisherObject(gamePublisher) {
//     return fetch(`../api/publishers/${gamePublisher}`,{
//         method: "GET",
//             headers: {
//             "Accept": "application/json"
//         }
//     }).then(response => response.json())
//         .then(data=>{return data})
// }
