const editBnt = document.getElementById("editBtn")
const publisherName = document.getElementById("name")
const hq = document.getElementById("hq")
const founded = document.getElementById("founded")
const deleteButtons = document.querySelectorAll('  .btn-outline-danger');
const saveBtn=document.getElementById("saveBtn")
const  title = document.querySelector("h1");
const publisherId = title.id;


editBnt.addEventListener("click", enableEdit)

/**
 * @param {Event} eventEdit
 */
function enableEdit(eventEdit){
hq.disabled=false;
    founded.disabled=false;
    publisherName.disabled=false;

    for (let i = 0; i < deleteButtons.length; i++) {
        deleteButtons[i].disabled = false;
    }
    editBnt.removeEventListener("click", enableEdit)
    editBnt.style.display="none";
    saveBtn.classList.remove('visually-hidden');
}



saveBtn.addEventListener("click", saveInfo)
/**
 * @param {Event} eventSave
 */
function saveInfo(eventSave){
    console.log( JSON.stringify({
        "publisherName" : publisherName.value,
        "headquarters" : hq.value,
        "founded" : founded.value,
    }))
    fetch(`/api/publishers/${publisherId}`, {
        method:"PUT",
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        body:     JSON.stringify({
            "name" : publisherName.value,
            "headquarters" : hq.value,
            "founded" : founded.value,
        })
    })
        .then(response => response.json())
        .then(data => console.log(data))
        .catch(error => console.error(error));
    }
