
// :not(.btn-outline-danger)
const deleteButtons = document.querySelectorAll('  .btn-outline-danger');
const allRows = document.querySelectorAll("#gamesTable tbody tr td:not(.del)");
const platformsDetailsAside = document.querySelector("aside");


for (const row of allRows) {
    row.addEventListener("click", toggleSelectedRow)
}

/**
 * @param {Event} event
 */
function toggleSelectedRow(event) {


    const activeRow = document.querySelectorAll("#gamesTable tbody tr.table-active")[0];

    if (event.target.tagName.toLowerCase() === "a") {
        return
    }

    /**
     * @type {HTMLElement}
     */
    const clickedRow = event.target.closest("tr")

    if (activeRow) {
        activeRow.classList.toggle("table-active")
    }

    if (activeRow !== clickedRow) {
        clickedRow.classList.toggle("table-active")
        platformsDetailsAside.style.visibility = "visible";
        fetchGamePlatforms(clickedRow.id)
    } else {
        platformsDetailsAside.style.visibility = "hidden";
    }

}

/**
 * @param {string} rowId
 */
function fetchGamePlatforms(rowId) {
    const gameId = +rowId.substring(rowId.indexOf('_') + 1);

    fetch(`api/games/${gameId}`)
        .then(handleGameResponse);

}

function handleGameResponse(response) {
    if (response.status === 404) {
        console.log("Error")
    } else {
        response.json()
            .then(processActualGameData)
    }
}

function processActualGameData(game) {
    clearStationStopTable();
    for (const platform of game.platformList) {
        addRowToPlatforms(platform);
    }
}

function addRowToPlatforms(platform) {
    const tableBody = document.querySelector("#platformsTable tbody");
    tableBody.innerHTML += `
                <tr>
                    <td>${platform.name}</td>
                    <td>${platform.developer}</td>
                    <td>${platform.generation}</td>
                </tr>`;
}

function clearStationStopTable() {
    const tableBody = document.querySelector("#platformsTable tbody");
    //tableBody.innerHTML = "";
    while (tableBody.children[0]) {
        tableBody.children[0].parentNode.removeChild(tableBody.children[0]);
    }
}




for (const deleteButton of deleteButtons) {
    deleteButton.addEventListener('click', deleteClicked);
}

function deleteClicked(event) {

    platformsDetailsAside.style.visibility = "hidden";

    const tableRow = event.target.parentNode.parentNode;
    const buttonId = event.target;
    const gameId = +buttonId.id;




    fetch(`/api/games/${gameId}`, {
        method: 'DELETE'
    })
        .then(handleDeletionResponse)

    tableRow.parentNode.removeChild(tableRow); // TODO: later!
}

function handleDeletionResponse(response) {
    if (response.status === 204) {
        console.log('Game Deleted');
    }
}

