const deleteButtons = document.querySelectorAll('tbody .btn-danger');

for (const deleteButton of deleteButtons) {
    deleteButton.addEventListener('click', deleteClicked);
}

function deleteClicked(event) {
    const tableRow = event.target.parentNode.parentNode;
    const stationStopId = +tableRow.id;

    fetch(`/api/stationstops/${stationStopId}`, {
        method: 'DELETE'
    })
        .then(handleDeletionResponse)

    tableRow.parentNode.removeChild(tableRow); // TODO: later!
}

function handleDeletionResponse(response) {
    if (response.status === 204) {
        console.log('It works');
    }
}