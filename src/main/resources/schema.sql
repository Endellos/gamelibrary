DROP table if exists GAMES;
DROP table if exists PUBLISHERS;
DROP table if exists PLATFORMS;
DROP table if exists GAME_PLATFORM;

create table PUBLISHERS
(
    ID             INTEGER auto_increment
        primary key
        unique,
    PUBLISHER_NAME VARCHAR,
    HEADQUARTERS   VARCHAR,
    FOUNDED        DATE
);
create table GAMES
(
    ID           INTEGER auto_increment
        primary key
        unique,
    NAME         CHARACTER VARYING not null,
    PUBLISHER   INTEGER  not null
        references PUBLISHERS,
    GENRE        CHARACTER VARYING,
    RELEASE_DATE DATE,
    COPIES_SOLD  int,
    PRICE        DOUBLE
);
CREATE TABLE PLATFORMS
(
    ID           INTEGER AUTO_INCREMENT PRIMARY KEY UNIQUE,
    NAME         VARCHAR,
    DEVELOPER    VARCHAR,
    RELEASE_DATE DATE,
    GENERATION   INTEGER
);
CREATE table GAME_PLATFORM
(
    GAME_ID     integer references GAMES,
    PLATFORM_ID integer references PLATFORMS
);