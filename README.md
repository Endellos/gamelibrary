# gameLibrary
Author: Margarita Stoyanova
Programming 3 project

## Domain Model
In the Domain model there are 3 entities: Game, Platform, Publisher.
### Relationships
Game-Publisher: Many-to-One
Game has a Publisher and Publisher has a list of games. Bidirectional!

Game-Platform: Many-to-Many
Game has a list of platforms and Platform has a list of games. Bidirectional!

![Entity Relation Diagram](src/main/resources/static/domainModel.PNG)

## Profiles
Use profiles to change between repository implementations!

collections: Use dataFactory class to "seed" the program. First and most basic implementation of the repository layer.
jdbc: Introduces a database. Makes use of jdbc template for interacting with the database. Uses schema.sql to create tables and data.sql to fill data in.
jpa: Uses a JPA implementation of repository layer. Uses annotation in domain classes to correctly map entities to tables. import.sql provides initial data to the database.
springData: Repository classes inherit from JpaRepository<T, ID> class. Because methods names have to follow convention a new implementation of the service layer had to be created.

## Database
The database used is an in-memory H2 database.
url: jdbc:h2:mem:gamelib
username: admin

## Disclaimers 
The website is available in English and Dutch, however the quality of the translation is questionable and mistakes are possible.
There is a favicon on the website, however it may have a problem appearing on browsers with a dark theme. Clearing the cache or using a different browser may solve the problem.
If a new version of Bootstrap is used make sure that you update the tags in the head of the html files with the correct version.
